import { Modal } from "antd";
import React, { useState } from "react";
import { dataGioHang } from "./dataGioHang";

export default function BtGioHangHook() {
  const [data, setData] = useState({ productList: dataGioHang });
  const [cart, setCart] = useState({ gioHang: [] });
  const [productDetail, setProductDetail] = useState({ chiTietSp: [] });
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };
  const hideModal = () => {
    setIsModalVisible(false);
  };
  const handleAddToCart = (sanPham) => {
    let cloneGioHang = [...cart.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP == sanPham.maSP;
    });
    if (index == -1) {
      let newSanPham = { ...sanPham, soLuong: 1 };
      cloneGioHang.push(newSanPham);
    } else {
      cloneGioHang[index].soLuong++;
    }
    setCart({ gioHang: cloneGioHang });
  };
  const handleDelProduct = (idSp) => {
    let cloneGioHang = [...cart.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP == idSp;
    });
    if (index !== -1) {
      cloneGioHang.splice(index, 1);
      setCart({ gioHang: cloneGioHang });
    }
  };
  const handleTangGiamSl = (idSp, giaTri) => {
    let cloneGioHang = [...cart.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP == idSp;
    });
    if (index !== -1) {
      cloneGioHang[index].soLuong += giaTri;
    }
    cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);
    setCart({ gioHang: cloneGioHang });
  };
  const handleShowDetail = (sanPham) => {
    let cloneSanPham = [...productDetail.chiTietSp];
    let index = cloneSanPham.findIndex((item) => {
      return item.maSP == sanPham.maSP;
    });
    if (index == -1) {
      let newSanPham = { ...sanPham };
      cloneSanPham.splice(0, 1, newSanPham);
    }
    setProductDetail({ chiTietSp: cloneSanPham });
  };

  return (
    <div className="container">
      {/* Header */}
      <h2 className="text-success pt-3">Bài tập giỏ hàng</h2>

      {/* Cart */}
      <div>
        <div className="text-right">
          <a
            className="text-danger font-weight-bold"
            onClick={() => {
              showModal();
            }}
          >
            Giỏ hàng: {cart.gioHang.length}
          </a>
        </div>
        <Modal
          title="Giỏ hàng"
          visible={isModalVisible}
          onOk={() => {
            hideModal();
          }}
          onCancel={() => {
            hideModal();
          }}
          width={1000}
        >
          <table className="table">
            <thead className="font-weight-bold">
              <td>Mã sản phẩm</td>
              <td>Hình ảnh</td>
              <td>Tên sản phẩm</td>
              <td>Số lượng</td>
              <td>Đơn giá</td>
              <td>Thành tiền</td>
              <td>Thao tác</td>
            </thead>
            <tbody>
              {cart.gioHang.map((item) => {
                return (
                  <tr>
                    <td>{item.maSP}</td>
                    <td className="w-25">
                      <img className="w-25" src={item.hinhAnh} />
                    </td>
                    <td>{item.tenSP}</td>
                    <td>
                      <button
                        onClick={() => {
                          handleTangGiamSl(item.maSP, 1);
                        }}
                        className="btn btn-light border-info py-0 px-1"
                      >
                        <i className="fa fa-plus text-secondary" />
                      </button>
                      <span className="mx-2">{item?.soLuong}</span>
                      <button
                        onClick={() => {
                          handleTangGiamSl(item.maSP, -1);
                        }}
                        className="btn btn-light border-info py-0 px-1"
                      >
                        <i className="fa fa-minus text-secondary" />
                      </button>
                    </td>
                    <td>{item.giaBan}</td>
                    <td>{item.giaBan * item?.soLuong}</td>
                    <td>
                      <button
                        onClick={() => {
                          handleDelProduct(item.maSP);
                        }}
                        className="btn btn-danger"
                      >
                        Xóa
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Modal>
      </div>

      {/* Product List */}
      <div className="row pb-3">
        {data.productList.map((item) => {
          return (
            <div className="card col mx-2 py-3 text-left">
              <img src={item.hinhAnh} alt="" className="card-img-top h-75" />
              <div className="card-body">
                <h5 className="card-title">{item.tenSP}</h5>
                <button
                  className="btn btn-info mr-2"
                  onClick={() => {
                    handleShowDetail(item);
                  }}
                >
                  Xem chi tiết
                </button>
                <button
                  className="btn btn-success"
                  onClick={() => {
                    handleAddToCart(item);
                  }}
                >
                  Thêm giỏ hàng
                </button>
              </div>
            </div>
          );
        })}
      </div>

      {/* Product Info */}
      {productDetail.chiTietSp.length > 0 && (
        <div>
          {productDetail.chiTietSp.map((item) => {
            return (
              <div className="row">
                <div className="col-4">
                  <h3>{item.tenSP}</h3>
                  <img className="img-fluid" src={item.hinhAnh} alt="" />
                </div>
                <div className="col-8 text-left">
                  <table className="table">
                    <tr>
                      <td className="font-weight-bold" colSpan={2}>
                        Thông số kỹ thuật
                      </td>
                    </tr>
                    <tr>
                      <td>Màn hình</td>
                      <td>{item.manHinh}</td>
                    </tr>
                    <tr>
                      <td>Hệ điều hành</td>
                      <td>{item.heDieuHanh}</td>
                    </tr>
                    <tr>
                      <td>Camera trước</td>
                      <td>{item.cameraTruoc}</td>
                    </tr>
                    <tr>
                      <td>Camera sau</td>
                      <td>{item.cameraSau}</td>
                    </tr>
                    <tr>
                      <td>RAM</td>
                      <td>{item.ram}</td>
                    </tr>
                    <tr>
                      <td>ROM</td>
                      <td>{item.rom}</td>
                    </tr>
                  </table>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}
